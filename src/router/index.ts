import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Tasks from "@/views/Tasks.vue";

Vue.use(VueRouter)

export interface RouteMetaData{
  meta:{
    icon : string;
    title : string;
  }
}

const routes: Array<RouteConfig & RouteMetaData> = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      icon: 'mdi-home-variant-outline',
      title: 'Home',
    },
  },
  {
    path: "/Tasks",
    name: "Tasks",
    component: Tasks,
    meta: {
      icon: 'mdi-checkbox-marked-circle-outline',
      title: 'Tasks',
    },
  },
  {
    // this route is just model for list of items in navigation drawer
    path: "/Board",
    name: "Inbox",
    component: Home,
    meta: {
      icon: 'mdi-bell-outline',
      title: 'Inbox',
    },
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
