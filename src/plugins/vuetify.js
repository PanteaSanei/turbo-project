import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        chipBlue: '#369cc3',
        chipPurple: '#6f66ac',
        chipGreen: '#378554',
        darkPurple: '#5C4E79',
        lightPurple: '#C895EF',
        todoChipPurple: '#9f46e1',
        todoChipBlue: '#4cd8fb',
        todoChipGreen: '#15d4c8',
        pink: '#d14a5c',
        lightPink: '#f75978',
        primary: '#6558f9',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#6faf53',
        yellow: '#FFC107',
      },
    },
  },
});
